package com.chuck.chucknorris.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.chuck.chucknorris.Constants.Constants

class PreferencesUtils(context: Context) : AppCompatActivity() {

    val context: Context = context


    lateinit var preferences: SharedPreferences

    private fun initPreferences() {
        preferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveSetPreferences(nameList: ArrayList<String>, namePreference: String) {
        Log.d(Constants.APP_NAME, Constants.logBuilder("saveSetPreferences", Constants.STRING_EMPTY))
        initPreferences()
        val editor = preferences.edit()
        var preferenceElement = getPreference(namePreference) ?: mutableSetOf()
        nameList.forEach { it: String ->
            preferenceElement.add(it)
        }

        editor.clear()
        editor.putStringSet(namePreference, preferenceElement)
        editor.apply()
    }

    fun savePreferences(name: String, namePreference: String) {
        Log.d(Constants.APP_NAME, Constants.logBuilder("savePreferences", Constants.STRING_EMPTY))
        initPreferences()
        val editor = preferences.edit()
        var preferenceElement = getPreference(namePreference) ?: mutableSetOf()

        preferenceElement.add(name)
        editor.clear()
        editor.putStringSet(namePreference, preferenceElement)
        editor.apply()
    }

    fun getPreference(namePreference: String): MutableSet<String>? {
        Log.d(Constants.APP_NAME, Constants.logBuilder("getPreference", Constants.STRING_EMPTY))
        initPreferences()
        val prfSetElement = preferences.getStringSet(namePreference, null)
        prfSetElement?.forEach { Log.d(Constants.APP_NAME, it) }
        return prfSetElement
    }

}