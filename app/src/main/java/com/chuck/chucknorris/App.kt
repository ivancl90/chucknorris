package com.chuck.chucknorris

import android.app.Application
import android.util.Log

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Log.d("CICE", "La aplicación se ha iniciado")
    }
}