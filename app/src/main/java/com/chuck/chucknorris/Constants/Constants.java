package com.chuck.chucknorris.Constants;

public class Constants {


    private Constants(){
        // default constructor
    }

    public static final String APP_NAME="CHUCK_NORRIS";
    public static final String CATEGORIES="Categories";
    public static final String JOKES="Jokes";
    public static final String STRING_EMPTY="";
    public static final String CATEGORY_NAME_PREFERENCES="CATEGORY_PREF";
    public static final String FAVORITES_JOKES="FAVORITES_JOKES";

    public static String logBuilder(String method, String extra){
        return String.format("Method: %1$s , desc: %2$s", method, extra);
    }

}
