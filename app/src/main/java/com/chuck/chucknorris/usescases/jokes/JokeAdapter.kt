package com.chuck.chucknorris.usescases.jokes

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chuck.chucknorris.Constants.Constants
import com.chuck.chucknorris.R
import com.chuck.chucknorris.data.PreferencesUtils
import com.chuck.chucknorris.usescases.jokes.models.Jokes
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_joke_field.view.*

class JokeAdapter(private val items: ArrayList<Jokes>, private val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_joke_field, p0, false), context)
    }


    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItems(items[p1])
    }

}


class ViewHolder(view: View, var context: Context) : RecyclerView.ViewHolder(view) {
    private var jokeList: ArrayList<Jokes>? = null

    fun bindItems(item: Jokes) {
        itemView.jokeText.text = item.value
        itemView.starFavorites.setOnClickListener {
            loadFavoritesJokesFromPreferences()
            var joke = itemView.jokeText.text.toString()
            if (jokeList != null) {
                var founded: Boolean = false
                jokeList!!.forEach { p ->
                    if (p.value.equals(joke)) {
                        itemView.starFavorites.setImageResource(R.drawable.star_off)
                        founded = true
                        removeJokeFromFavorites(joke)
                    }
                }
                if (!founded) {
                    itemView.starFavorites.setImageResource(R.drawable.star_on)
                    PreferencesUtils(context).savePreferences(joke, Constants.FAVORITES_JOKES)
                }
            } else {
                itemView.starFavorites.setImageResource(R.drawable.star_on)
                PreferencesUtils(context).savePreferences(joke, Constants.FAVORITES_JOKES)
            }

        }

    }

    private fun loadFavoritesJokesFromPreferences() {
        if (jokeList == null) {
            var jokeSet: MutableSet<String>? = PreferencesUtils(context).getPreference(Constants.FAVORITES_JOKES)
            if (jokeSet != null)
                jokeList = jokeArrayFromSet(jokeSet!!)
        }
    }

    private fun removeJokeFromFavorites(joke: String) {
        if (jokeList == null) {
            loadFavoritesJokesFromPreferences()
        }
        var jokeRemove: Jokes? = null
        jokeList!!.forEach { it ->
            if (it.value.equals(joke))
                jokeRemove = it
        }
        jokeList!!.remove(jokeRemove)
        PreferencesUtils(context).saveSetPreferences(jokeArray(joke), Constants.FAVORITES_JOKES)
    }

    private fun jokeArrayFromSet(jokeSet: MutableSet<String>): ArrayList<Jokes> {
        var items = arrayListOf<Jokes>()
        if (jokeList == null)
            jokeSet!!.toList().forEach { it: String ->
                items.add(Jokes(null, null, null, null, it))
            }
        jokeList = items
        return items
    }

    private fun jokeArray(joke: String): ArrayList<String> {
        var items = arrayListOf<String>()
        if (jokeList == null)
            jokeList!!.toList().forEach { it ->
                items.add(Gson().toJson(it))
            }
        return items
    }
}
