package com.chuck.chucknorris.usescases.splash

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.chuck.chucknorris.Constants.Constants
import com.chuck.chucknorris.R
import com.chuck.chucknorris.config.ConfigProperties
import com.chuck.chucknorris.data.PreferencesUtils
import com.chuck.chucknorris.usescases.categories.CategoryActivity
import com.chuck.chucknorris.usescases.categories.models.Categories
import com.chuck.chucknorris.usescases.globals.GlobalActivity
import com.google.gson.Gson
import java.net.URL

class SplashActivity : GlobalActivity() {

    var categoryList : Categories? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        getCategories()
    }

    private fun getCategories() {
        var result: String
        Thread {
            result = URL(ConfigProperties.getValue("url.categories", this.applicationContext)).readText()
            this.runOnUiThread {
                Log.d("CATEGORIAS RECIBIDAS", result)

                categoryList = Gson().fromJson(result, Categories::class.java)
                Log.d(Constants.APP_NAME, categoryList!!.size.toString())
                loadCategoriesIntoPreferences()
                var intent = Intent(this@SplashActivity, CategoryActivity::class.java)
                startActivity(intent)
            }
        }.start()
    }

    private fun loadCategoriesIntoPreferences(){
        if(categoryList!=null){
            PreferencesUtils(this.applicationContext).saveSetPreferences(categoryList!!, Constants.CATEGORY_NAME_PREFERENCES)
        }
    }



}