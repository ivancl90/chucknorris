package com.chuck.chucknorris.usescases.globals

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.chuck.chucknorris.extension.log

abstract class GlobalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("CICE", localClassName)
        localClassName.log()
    }
}