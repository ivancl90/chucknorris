package com.chuck.chucknorris.usescases.categories

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.chuck.chucknorris.Constants.Constants.CATEGORY_NAME_PREFERENCES
import com.chuck.chucknorris.Constants.Constants.FAVORITES_JOKES
import com.chuck.chucknorris.R
import com.chuck.chucknorris.data.PreferencesUtils
import com.chuck.chucknorris.usescases.globals.GlobalActivity
import com.chuck.chucknorris.usescases.jokes.JokeAdapter
import com.chuck.chucknorris.usescases.jokes.models.Jokes
import kotlinx.android.synthetic.main.activity_category.*

class CategoryActivity : GlobalActivity() {

    var categoryList: MutableSet<String>? = null
    var jokeSet: MutableSet<String>? = null
    private lateinit var pUtils: PreferencesUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        recyclerGrid.layoutManager = LinearLayoutManager(this)
        pUtils = PreferencesUtils(this.applicationContext)
        if (categoryList == null) {
            categoryList = pUtils.getPreference(CATEGORY_NAME_PREFERENCES)
            recyclerGrid.adapter = CategoryAdapter(category(), this@CategoryActivity)
        }
        showCategories()
        showFavorites()
    }


    private fun showCategories() {
        categoryButton.setOnClickListener {
            recyclerGrid.layoutManager = LinearLayoutManager(this)
            pUtils = PreferencesUtils(this.applicationContext)
            if (categoryList == null) {
                categoryList = pUtils.getPreference(CATEGORY_NAME_PREFERENCES)

            }
            recyclerGrid.adapter = CategoryAdapter(category(), this@CategoryActivity)
        }

    }


    private fun showFavorites() {
        favoriteButton.setOnClickListener {
            recyclerFavoriteGrid.layoutManager = LinearLayoutManager(this)
            pUtils = PreferencesUtils(this.applicationContext)
            jokeSet = pUtils.getPreference(FAVORITES_JOKES)
            if (jokeSet != null) {
                recyclerGrid.adapter = JokeAdapter(jokeArrayFromSet(jokeSet), this@CategoryActivity)
            } else {
                Toast.makeText(this, "No has añadido bromas a tus favoritos", Toast.LENGTH_SHORT).show()
                showCategories()
            }
        }
    }

    private fun category(): ArrayList<String> {
        var items = arrayListOf<String>()
        categoryList!!.toList().forEach { it: String ->
            items.add(it)
        }
        return items
    }

    private fun jokeArrayFromSet(jokeSet: MutableSet<String>?): ArrayList<Jokes> {
        var items = arrayListOf<Jokes>()
        if (jokeSet != null)
            jokeSet!!.toList().forEach { it: String ->
                items.add(Jokes(null, null, null, null, it))
            }

        return items
    }

}
