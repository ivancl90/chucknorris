package com.chuck.chucknorris.usescases.jokes

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.chuck.chucknorris.Constants.Constants
import com.chuck.chucknorris.R
import com.chuck.chucknorris.config.ConfigProperties
import com.chuck.chucknorris.data.PreferencesUtils
import com.chuck.chucknorris.usescases.globals.GlobalActivity
import com.chuck.chucknorris.usescases.jokes.models.Jokes
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_joke.*
import java.net.URL

class JokeActivity : GlobalActivity() {

    var joke: Jokes? = null
    var jokeList: ArrayList<Jokes>?=null
    private lateinit var pUtils: PreferencesUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_joke)
        recyclerJokeGrid.layoutManager = LinearLayoutManager(this)
        getJokeFromCategory()
        getAnotherJoke()
    }

    private fun getAnotherJoke(){
        anotherButton.setOnClickListener {
            getJokeFromCategory()
        }
    }

    private fun getJokeFromCategory() {
        var result: String
        Thread {
            result = URL(
                ConfigProperties.getValue(
                    "url.random.joke.categories",
                    this.applicationContext
                ) + intent.getStringExtra("category")
            ).readText()
            this.runOnUiThread {
                Log.d("BROMA RECIBIDAS", result)

                joke = Gson().fromJson(result, Jokes::class.java)
                Log.d(Constants.APP_NAME, joke!!.toString())
                recyclerJokeGrid.adapter = JokeAdapter(jokeArray(joke!!), this@JokeActivity)
            }
        }.start()
    }

    private fun jokeArray(joke:Jokes): ArrayList<Jokes> {
        var items = arrayListOf<Jokes>()
        if (jokeList != null)
            jokeList!!.toList().forEach {it->
                items.add(it)
            }
        else
            items.add(joke)
        return items
    }
}