package com.chuck.chucknorris.usescases.categories

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chuck.chucknorris.R
import com.chuck.chucknorris.usescases.jokes.JokeActivity
import kotlinx.android.synthetic.main.activity_category.view.*
import kotlinx.android.synthetic.main.item_category_field.view.*


class CategoryAdapter(private val items: ArrayList<String>,private  val context: Context) : RecyclerView.Adapter<ViewHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_category_field, p0, false))
    }


    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItems(items[p1], context)
    }

}


class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(item: String, context: Context) {
        itemView.textText.text = item
        itemView.categoryItem.setOnClickListener {
            var intent = Intent(context, JokeActivity::class.java)
            var category: String = item
            intent.putExtra("category", category)
            context.startActivity(intent)
        }

    }

}
